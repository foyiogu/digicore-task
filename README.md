# Digicore bank App
This is a simple REST API for a simple Banking Application.  

### Technologies
- Java
- Maven
- Springboot
- Spring Security


### Requirements

You need the following to build and run the application:

- [JDK 11](https://www.oracle.com/java/technologies/javase-jdk11-downloads.html)
- [Maven 3.8.1](https://maven.apache.org) (Optional as code already contains maven wrapper)


## How to run
### step 1 - clone project with from [here](https://gitlab.com/foyiogu/digicore-task.git)

```
git clone https://gitlab.com/foyiogu/digicore-task.git
```


### step 2 - move into the project directory
```
cd digicore-task/
```

### step 3 - Generate the .jar file
```
mvn clean install 
OR
./mvnw clean install
```

### step 4 - run the project
```
Algorithms are in the algorithms package
OR
Full Web Application is in the webApplication package
```


