package com.francis.digicoretask.algorithms;

import java.util.Arrays;

public class OverallScore {
    public static void main(String[] args) {
        int [] tom = {1, 4, 7, 2, 4};
        int [] jack = {3, 4, 2, 4, 4};
        System.out.println(Arrays.toString(overallScore(tom, jack)));
    }

    /**
     *In a game of table tennis, two players play against each other in a round of 5.
     * The person with the higher score in round of 5 is leading the other by 1 point.
     * Given a series of output between two players Tom and Jack in table tennis.
     * Output the overall score point by each player in the right order.
     * a. NB: No point is awarded when both players have same score for same
     * round of game
     * b. Input Tom [1, 4, 7, 2, 4] Jack [3, 4, 2, 4, 4] Output [1, 2] Tom led Jack
     * in 1 game, Jack led Tom in 2 games and they had an equal score in 2
     * games. Hence our output is [1, 2] with Tom’s score coming first.
     */
    public static int[] overallScore(int [] playerA, int [] playerB ){

        //check for unequal array sizes
        if (playerA.length != playerB.length ) return null;

        int playerAWins = 0;
        int playerBWins = 0;

        for (int i = 0; i < playerA.length; i++) {
            if (playerA[i] > playerB[i]){
                playerAWins++;
            }else if (playerB[i] > playerA[i]){
                playerBWins++;
            }
        }
        //count of player wins
        return new int[]{playerAWins,playerBWins};
    }
}
