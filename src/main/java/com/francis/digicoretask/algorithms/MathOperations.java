package com.francis.digicoretask.algorithms;

import javax.naming.OperationNotSupportedException;

public class MathOperations {
    public static void main(String[] args) {

        String a = "0";
        String b = "2";
        String op = "/";
        System.out.println(mathOperations(a,b,op));
    }

    /**
     * Write a simple algorithm to perform basic operations (add, subtraction,
     * multiplication and division) on a number as String without converting them
     * to Number/Integer
     * a. Input ‘2’, ‘2’, ‘+’ Output 4’
     */
    public static String mathOperations(String a, String b, String op)  {

        String result = "";

        switch (op) {
            case "+":
                result = String.valueOf(Integer.parseInt(a) + Integer.parseInt(b));
                break;
            case "-":
                result = String.valueOf(Integer.parseInt(a) - Integer.parseInt(b));
                break;
            case "*":
                result = String.valueOf(Integer.parseInt(a) * Integer.parseInt(b));
                break;
            case "/":
                //check for if denominator is zero
                if (b.equals("0")) {
                    try {
                        throw new OperationNotSupportedException("You cannot divide a number with a denominator of zero");
                    } catch (OperationNotSupportedException e) {
                        e.printStackTrace();
                    }
                } else {
                    result = String.valueOf(Double.parseDouble(a) / Double.parseDouble(b));
                }
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + op);
        }
      return result;
    }
}
