package com.francis.digicoretask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DigicoreTaskApplication {

    public static void main(String[] args) {
        SpringApplication.run(DigicoreTaskApplication.class, args);
    }

}
