package com.francis.digicoretask.webApplication.security;

import com.francis.digicoretask.webApplication.DAO.AccountDAO;
import com.francis.digicoretask.webApplication.exceptions.ApiResourceNotFoundException;
import com.francis.digicoretask.webApplication.models.Account;
import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Optional;

@Service
@AllArgsConstructor
public class MyUserDetailsService implements UserDetailsService {

    private AccountDAO accountDao;

    @Override
    public UserDetails loadUserByUsername(String accountNumber) {
        int index = accountDao.findUserIndexByAccountNumber(null, accountNumber);

        if (index < 0) throw new ApiResourceNotFoundException("Oops account not found !!!");

        Optional<Account> account = accountDao.find(index);

        return org.springframework.security.core.userdetails.User
                .withUsername(account.get().getAccountNumber())
                .password(account.get().getPassword())
                .authorities(new ArrayList<>())
                .accountExpired(false)
                .accountLocked(false)
                .credentialsExpired(false)
                .disabled(false)
                .build();
    }
}