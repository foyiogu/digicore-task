package com.francis.digicoretask.webApplication.service;

import com.francis.digicoretask.webApplication.payload.request.DepositRequest;
import com.francis.digicoretask.webApplication.payload.request.WithdrawRequest;
import com.francis.digicoretask.webApplication.payload.response.AccountInfoResponse;
import com.francis.digicoretask.webApplication.payload.response.AcctHistoryResponse;
import com.francis.digicoretask.webApplication.payload.response.Response;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface AccountService {

    ResponseEntity<AccountInfoResponse> getAccountInformation(String accountNumber);
    ResponseEntity<List<AcctHistoryResponse>> getAccountStatement(String accountNumber);
    ResponseEntity<Response> withdraw(WithdrawRequest withdrawRequest);
    ResponseEntity<Response> deposit(DepositRequest depositRequest);
}
