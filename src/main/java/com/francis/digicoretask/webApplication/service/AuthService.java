package com.francis.digicoretask.webApplication.service;

import com.francis.digicoretask.webApplication.payload.request.CreateAccountRequest;
import com.francis.digicoretask.webApplication.payload.request.LoginRequest;
import com.francis.digicoretask.webApplication.payload.response.CreateAccountResponse;
import com.francis.digicoretask.webApplication.payload.response.LoginResponse;
import org.springframework.http.ResponseEntity;

public interface AuthService {

    ResponseEntity<CreateAccountResponse> createAccount(CreateAccountRequest accountRequest) throws Exception;
    ResponseEntity<LoginResponse> userLogin(LoginRequest loginRequest) throws Exception;
}
