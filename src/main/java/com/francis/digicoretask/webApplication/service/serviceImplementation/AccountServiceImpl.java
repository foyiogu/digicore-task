package com.francis.digicoretask.webApplication.service.serviceImplementation;

import com.francis.digicoretask.webApplication.DAO.AccountDAO;
import com.francis.digicoretask.webApplication.DAO.AccountHistoryDAO;
import com.francis.digicoretask.webApplication.exceptions.ApiBadRequestException;
import com.francis.digicoretask.webApplication.exceptions.ApiResourceNotFoundException;
import com.francis.digicoretask.webApplication.models.Account;
import com.francis.digicoretask.webApplication.models.AccountHistory;
import com.francis.digicoretask.webApplication.payload.request.DepositRequest;
import com.francis.digicoretask.webApplication.payload.request.WithdrawRequest;
import com.francis.digicoretask.webApplication.payload.response.AccountInfoResponse;
import com.francis.digicoretask.webApplication.payload.response.AccountResponse;
import com.francis.digicoretask.webApplication.payload.response.AcctHistoryResponse;
import com.francis.digicoretask.webApplication.payload.response.Response;
import com.francis.digicoretask.webApplication.service.AccountService;
import com.francis.digicoretask.webApplication.utils.HelperClass;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
@AllArgsConstructor
public class AccountServiceImpl implements AccountService {

    private final AccountDAO accountDao;
    private final AccountHistoryDAO accountHistoryDoa;
    private AuthenticationManager authenticationManager;

    public ResponseEntity<AccountInfoResponse> getAccountInformation(String accountNumber){
        AccountInfoResponse accountInfoResponse = new AccountInfoResponse();
        AccountResponse accountResponse = new AccountResponse();

        int index = accountDao.findUserIndexByAccountNumber(null, accountNumber);

        if(index >= 0) {
            Account account = accountDao.find(index).orElseThrow(()->{
                throw new IllegalArgumentException("Account not found");
            });
            accountResponse.setAccountName(account.getAccountName());
            accountResponse.setAccountNumber(account.getAccountNumber());
            accountResponse.setBalance(account.getBalance());

            accountInfoResponse.setAccountResponse(accountResponse);
        }else{
            throw new ApiResourceNotFoundException("Oops account not found !!!");
        }

        accountInfoResponse.setResponseCode(200);
        accountInfoResponse.setSuccess(true);
        accountInfoResponse.setMessage("Successfully fetched account details");

        return new ResponseEntity<>(accountInfoResponse, HttpStatus.CREATED);
    }

    public ResponseEntity<List<AcctHistoryResponse>> getAccountStatement(String accountNumber){
        return new ResponseEntity<>(accountHistoryDoa.getAccountHistory(accountNumber), HttpStatus.OK);
    }

    public ResponseEntity<Response> withdraw(WithdrawRequest withdrawRequest){

        try{
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(withdrawRequest.getAccountNumber(), withdrawRequest.getAccountPassword()));
        }catch (BadCredentialsException e){
            throw new ApiBadRequestException("Oops incorrect account number or password");
        }

        if(withdrawRequest.getWithdrawnAmount() < 1.0) throw new ApiBadRequestException("Oops invalid amount");

        Response res = new Response();
        Date currentDate = HelperClass.generateCurrentDateTime();

        int index = accountDao.findUserIndexByAccountNumber(null, withdrawRequest.getAccountNumber());

        if(index >= 0) {
            Account account = accountDao.find(index).orElseThrow(()->{
                throw new IllegalArgumentException("Account not found");
            });
            Double currentBalance = account.getBalance() - withdrawRequest.getWithdrawnAmount();
            double minimumBalance = 500.0;
            if(currentBalance < minimumBalance) throw new ApiBadRequestException("Oops cannot withdraw below #500.00");

            //set account balance
            account.setBalance(currentBalance);
            accountDao.save(account);

            //set account history
            AccountHistory accountHistory = new AccountHistory();
            accountHistory.setAccountNumber(withdrawRequest.getAccountNumber());
            accountHistory.setAmount(withdrawRequest.getWithdrawnAmount());
            accountHistory.setAccountBalance(currentBalance);
            accountHistory.setNarration(withdrawRequest.getNarration());
            accountHistory.setTransactionDate(currentDate);
            accountHistory.setTransactionType("Withdrawal");
            accountHistoryDoa.save(accountHistory);

        }else{
            throw new ApiResourceNotFoundException("This account can not be found !!!");
        }

        res.setMessage("Successfully withdrawn amount");
        res.setSuccess(true);
        res.setResponseCode(201);

        return new ResponseEntity<>(res, HttpStatus.CREATED);
    }

    public ResponseEntity<Response> deposit(DepositRequest depositRequest){
        Response res = new Response();
        Date currentDate = HelperClass.generateCurrentDateTime();

        Double maximumDeposit = 1000000.0;
        Double minimumDeposit = 1.0;

        if(depositRequest.getAmount() >= maximumDeposit || depositRequest.getAmount() <= minimumDeposit)
            throw new ApiBadRequestException("Oops invalid amount to deposit");

        //find account by account number
        int index = accountDao.findUserIndexByAccountNumber(null, depositRequest.getAccountNumber());

        if(index >= 0) {
            Account account = accountDao.find(index).orElseThrow(()->{
                throw new IllegalArgumentException("Account not found");
            });

            //set account history
            AccountHistory accountHistory = new AccountHistory();
            accountHistory.setAccountNumber(depositRequest.getAccountNumber());
            accountHistory.setAmount(depositRequest.getAmount());
            accountHistory.setAccountBalance(account.getBalance() + depositRequest.getAmount());
            accountHistory.setNarration(depositRequest.getNarration());
            accountHistory.setTransactionDate(currentDate);
            accountHistory.setTransactionType("Deposit");
            accountHistoryDoa.save(accountHistory);

            //set account balance
            account.setBalance(account.getBalance() + depositRequest.getAmount());
            accountDao.save(account);

        }else{
            throw new ApiResourceNotFoundException("Oops account not found !!!");
        }

        res.setMessage("Successfully deposited amount");
        res.setSuccess(true);
        res.setResponseCode(201);

        return new ResponseEntity<>(res, HttpStatus.CREATED);

    }
}
