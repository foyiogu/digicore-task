package com.francis.digicoretask.webApplication.service.serviceImplementation;

import com.francis.digicoretask.webApplication.DAO.AccountDAO;
import com.francis.digicoretask.webApplication.DAO.AccountHistoryDAO;
import com.francis.digicoretask.webApplication.exceptions.ApiBadRequestException;
import com.francis.digicoretask.webApplication.exceptions.ApiConflictException;
import com.francis.digicoretask.webApplication.models.Account;
import com.francis.digicoretask.webApplication.models.AccountHistory;
import com.francis.digicoretask.webApplication.payload.request.CreateAccountRequest;
import com.francis.digicoretask.webApplication.payload.request.LoginRequest;
import com.francis.digicoretask.webApplication.payload.response.CreateAccountResponse;
import com.francis.digicoretask.webApplication.payload.response.LoginResponse;
import com.francis.digicoretask.webApplication.security.JwtUtils;
import com.francis.digicoretask.webApplication.security.MyUserDetailsService;
import com.francis.digicoretask.webApplication.service.AuthService;
import com.francis.digicoretask.webApplication.utils.HelperClass;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
@AllArgsConstructor
public class AuthServiceImpl implements AuthService {

    private AccountDAO accountDao;
    private AuthenticationManager authenticationManager;
    private MyUserDetailsService myUserDetailsService;
    private JwtUtils jwtUtils;
    private PasswordEncoder encoder;
    private AccountHistoryDAO accountHistoryDoa;

    public ResponseEntity<CreateAccountResponse> createAccount(CreateAccountRequest accountRequest) {

        if(accountRequest.getInitialDeposit() < 500)
            throw new ApiBadRequestException("Oops cannot create account with initial deposit less than #500");

        int index = accountDao.findUserIndexByAccountName(accountRequest.getAccountName());

        if(index >= 0)
            throw new ApiConflictException("Oops account name already exist");

        //create random account number
        String accountNumber = HelperClass.generateNewAccountNumber();
        System.err.println("ACCOUNT NUMBER IS "+accountNumber);

        Account account = new Account();
        account.setAccountName(accountRequest.getAccountName());
        account.setAccountNumber(accountNumber);
        account.setPassword(encoder.encode(accountRequest.getAccountPassword()));
        account.setBalance(accountRequest.getInitialDeposit());

        accountDao.save(account);

        AccountHistory accountHistory = new AccountHistory();

        accountHistory.setAccountBalance(account.getBalance());
        accountHistory.setTransactionType("Initial Deposit");
        accountHistory.setNarration("Deposit");
        accountHistory.setTransactionDate(new Date());
        accountHistory.setAmount(account.getBalance());
        accountHistory.setAccountNumber(accountNumber);

        accountHistoryDoa.save(accountHistory);

        CreateAccountResponse res = new CreateAccountResponse();
        res.setMessage("Successfully created account");
        res.setSuccess(true);
        res.setResponseCode(201);
        res.setAccountNumber(accountNumber);

        return new ResponseEntity<>(res, HttpStatus.CREATED);
    }

    public ResponseEntity<LoginResponse> userLogin(LoginRequest loginRequest) {

        try{
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(loginRequest.getAccountNumber(), loginRequest.getAccountPassword()));
        }catch (BadCredentialsException e){
            throw new ApiBadRequestException("Oops incorrect account number or password");
        }

        final UserDetails userDetails = myUserDetailsService.loadUserByUsername(loginRequest.getAccountNumber());
        final String jwt = jwtUtils.generateToken(userDetails);

        LoginResponse res = new LoginResponse();

        Account account = new Account();
        account.setAccountNumber(loginRequest.getAccountNumber());
        account.setPassword(loginRequest.getAccountPassword());

        res.setAccessToken("204");
        res.setSuccess(true);
        res.setAccessToken(jwt);

        return new ResponseEntity<>(res, HttpStatus.ACCEPTED);

    }
}
