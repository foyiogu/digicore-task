package com.francis.digicoretask.webApplication.controller;

import com.francis.digicoretask.webApplication.payload.request.DepositRequest;
import com.francis.digicoretask.webApplication.payload.request.WithdrawRequest;
import com.francis.digicoretask.webApplication.payload.response.AccountInfoResponse;
import com.francis.digicoretask.webApplication.payload.response.AcctHistoryResponse;
import com.francis.digicoretask.webApplication.payload.response.Response;
import com.francis.digicoretask.webApplication.service.AccountService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@AllArgsConstructor
public class AccountController {

    private final AccountService accountService;

    @GetMapping(path="/account_info/{accountNumber}")
    public ResponseEntity<AccountInfoResponse> getAccountInformation(@PathVariable String accountNumber){
        return accountService.getAccountInformation(accountNumber);
    }

    @GetMapping(path="/account_statement/{accountNumber}")
    public ResponseEntity<List<AcctHistoryResponse>> getAccountStatement(@PathVariable String accountNumber){
        return accountService.getAccountStatement(accountNumber);
    }

    @PostMapping(path="/withdrawal")
    public ResponseEntity<Response> withdraw(@RequestBody WithdrawRequest withdrawReq){
        return accountService.withdraw(withdrawReq);
    }

    @PostMapping(path="/deposit")
    public ResponseEntity<Response> deposit(@RequestBody DepositRequest depositReq){
        return accountService.deposit(depositReq);
    }
}
