package com.francis.digicoretask.webApplication.controller;

import com.francis.digicoretask.webApplication.payload.request.CreateAccountRequest;
import com.francis.digicoretask.webApplication.payload.request.LoginRequest;
import com.francis.digicoretask.webApplication.payload.response.CreateAccountResponse;
import com.francis.digicoretask.webApplication.payload.response.LoginResponse;
import com.francis.digicoretask.webApplication.service.AuthService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@AllArgsConstructor
public class AuthController {

    private final AuthService authService;

    @PostMapping(path="/create_account")
    public ResponseEntity<CreateAccountResponse> createAccount(@RequestBody CreateAccountRequest accountReq) throws Exception {
        return authService.createAccount(accountReq);
    }

    @PostMapping(path="/login")
    public ResponseEntity<LoginResponse> userLogin(@RequestBody LoginRequest loginReq) throws Exception {
        return authService.userLogin(loginReq);
    }
}
