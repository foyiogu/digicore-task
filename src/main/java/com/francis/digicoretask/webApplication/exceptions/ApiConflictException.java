package com.francis.digicoretask.webApplication.exceptions;

public class ApiConflictException extends RuntimeException{
    public ApiConflictException(String message) {
        super(message);
    }
}
