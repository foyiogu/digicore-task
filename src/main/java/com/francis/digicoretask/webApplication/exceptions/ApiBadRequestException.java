package com.francis.digicoretask.webApplication.exceptions;

public class ApiBadRequestException extends RuntimeException{

    public ApiBadRequestException(String message) {
        super(message);
    }
}
