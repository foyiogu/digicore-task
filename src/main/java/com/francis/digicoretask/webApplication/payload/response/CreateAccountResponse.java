package com.francis.digicoretask.webApplication.payload.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreateAccountResponse {
    private int responseCode;
    private boolean success;
    private String message;
    private String accountNumber;
}
