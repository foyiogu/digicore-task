package com.francis.digicoretask.webApplication.payload.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.NotBlank;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class CreateAccountRequest {

    @NotBlank(message = "AccountName is mandatory")
    private String accountName;

    @NotBlank(message = "Password  is mandatory")
    private String accountPassword;

    private Double initialDeposit;
}
