package com.francis.digicoretask.webApplication.DAO;

import com.francis.digicoretask.webApplication.models.AccountHistory;
import com.francis.digicoretask.webApplication.payload.response.AcctHistoryResponse;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class AccountHistoryDAO implements Dao<AccountHistory, Map> {

    private Map<String, List<AcctHistoryResponse>> accountInformation = new HashMap<>();

    @Override
    public Optional find(int index) {
        return Optional.ofNullable(accountInformation.get(index));
    }

    @Override
    public Map findAll() {
        return accountInformation;
    }

    @Override
    public AccountHistory save(AccountHistory accountHistory) {


        AcctHistoryResponse acctHistoryResponse = new AcctHistoryResponse();
        acctHistoryResponse.setAccountBalance(accountHistory.getAccountBalance());
        acctHistoryResponse.setAmount(accountHistory.getAmount());
        acctHistoryResponse.setNarration(accountHistory.getNarration());
        acctHistoryResponse.setTransactionDate(accountHistory.getTransactionDate());
        acctHistoryResponse.setTransactionType(accountHistory.getTransactionType());

        if(accountInformation.containsKey(accountHistory.getAccountNumber())){
            List<AcctHistoryResponse> info2 = accountInformation.get(accountHistory.getAccountNumber());
            info2.add(acctHistoryResponse);
            accountInformation.put(accountHistory.getAccountNumber(),
                    info2);
        }else{

            List<AcctHistoryResponse> info = new ArrayList<>();
            info.add(acctHistoryResponse);
            accountInformation.put(accountHistory.getAccountNumber(),
                    info);
        }

        return accountHistory;
    }

    @Override
    public void delete(AccountHistory o) {
        accountInformation.remove(o);
    }

    public List<AcctHistoryResponse> getAccountHistory(String accountNumber){
        System.out.println(accountInformation.get(accountNumber));
        return accountInformation.get(accountNumber) != null ? accountInformation.get(accountNumber): new ArrayList<>();
    }
}
