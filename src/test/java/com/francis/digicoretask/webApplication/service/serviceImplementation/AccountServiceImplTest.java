package com.francis.digicoretask.webApplication.service.serviceImplementation;

import com.francis.digicoretask.webApplication.DAO.AccountDAO;
import com.francis.digicoretask.webApplication.DAO.AccountHistoryDAO;
import com.francis.digicoretask.webApplication.models.Account;
import com.francis.digicoretask.webApplication.models.AccountHistory;
import com.francis.digicoretask.webApplication.payload.request.DepositRequest;
import com.francis.digicoretask.webApplication.payload.request.WithdrawRequest;
import com.francis.digicoretask.webApplication.payload.response.AccountInfoResponse;
import com.francis.digicoretask.webApplication.payload.response.AccountResponse;
import com.francis.digicoretask.webApplication.payload.response.AcctHistoryResponse;
import com.francis.digicoretask.webApplication.payload.response.Response;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.AuthenticationException;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AccountServiceImplTest {

    @Mock
    private AccountDAO mockAccountDao;
    @Mock
    private AccountHistoryDAO mockAccountHistoryDoa;
    @Mock
    private AuthenticationManager mockAuthenticationManager;

    private AccountServiceImpl accountServiceImplUnderTest;

    @BeforeEach
    void setUp() {
        accountServiceImplUnderTest = new AccountServiceImpl(mockAccountDao, mockAccountHistoryDoa, mockAuthenticationManager);
    }

    @Test
    void testGetAccountStatement() {
        // Setup
        final ResponseEntity<List<AcctHistoryResponse>> expectedResult = new ResponseEntity<>(List.of(new AcctHistoryResponse(new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "transactionType", "narration", 0.0, 0.0)), HttpStatus.OK);

        // Configure AccountHistoryDAO.getAccountHistory(...).
        final List<AcctHistoryResponse> acctHistoryResponses = List.of(new AcctHistoryResponse(new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "transactionType", "narration", 0.0, 0.0));
        when(mockAccountHistoryDoa.getAccountHistory("accountNumber")).thenReturn(acctHistoryResponses);

        // Run the test
        final ResponseEntity<List<AcctHistoryResponse>> result = accountServiceImplUnderTest.getAccountStatement("accountNumber");

        // Verify the results
        assertThat(result).isEqualTo(expectedResult);
    }

    @Test
    void testGetAccountStatement_AccountHistoryDAOReturnsNoItems() {
        // Setup
        final ResponseEntity<List<AcctHistoryResponse>> expectedResult = new ResponseEntity<>(List.of(new AcctHistoryResponse(new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "transactionType", "narration", 0.0, 0.0)), HttpStatus.OK);
        when(mockAccountHistoryDoa.getAccountHistory("accountNumber")).thenReturn(Collections.emptyList());

        // Run the test
        final ResponseEntity<List<AcctHistoryResponse>> result = accountServiceImplUnderTest.getAccountStatement("accountNumber");

        // Verify the results
        assertThat(result).isNotEqualTo(expectedResult);
    }

}
